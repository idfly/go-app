Go lang container with ruby
===========================

Contains ruby for running guard in order to execute tests and compile
application automatically on file change.

You should place your gems under /app/.build/vendor/bundle path or set proper
GEM_PATH and PATH in order to access gem and gem bin files.
